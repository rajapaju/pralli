(ns ball.core
  (:require [reagent.core :as reagent]
            [reagent.session :as session]
            [secretary.core :as secretary :include-macros true]
            [monet.canvas :as canvas]
            [goog.events :as events]
            [goog.events.EventType :as ET]
            [goog.history.EventType :as EventType]
            [cljs.core.async :as async
             :refer [<! >! chan put! timeout alts!]]
            [ball.image :as img])
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:import goog.History
           goog.events))

(defn b2vec2 [x y] (js/Box2D.Common.Math.b2Vec2. x y))


(session/put! :engine-speed 1.5)
(session/put! :steering-angle 0)

(def MAX-STEER-ANGLE (/ Math/PI 3))

(def SCALE 100)
(def canvas-attributes {:id "c" :width (- (aget js/window "innerWidth") SCALE) :height (- (aget js/window "innerHeight") SCALE)})

(session/put! :motion-event :init)
(session/put! :motion {:vx 0 :vy 0})
(session/put! :scores {:west 0 :east 0})

(def motion-chan (chan 1))

(def car-image (img/create-image "img/aston.png"))

(defn update-image [ctx body image]
  (let [x (aget (.GetPosition body) "x")
        y (aget (.GetPosition body) "y")
        angle (.GetAngle body)]
    (doto ctx
      (.save)
      (.translate (* x SCALE) (- (:height canvas-attributes) (* y SCALE)))
      (.rotate (* -1 angle))
      (.scale 1.2 1)
      (.drawImage image (* -1 (/ (aget image "width") 2)) (* -1 (/ (aget image "height") 2)))
      (.restore)))
  )

(defn update-car-image [{context :context {car-body :body} :car}]
  (update-image context car-body car-image))

(defn motion [e]
  (let [
        vx-new (* -5 (aget (aget e "accelerationIncludingGravity") "x"))
        vy-new (* -5 (aget (aget e "accelerationIncludingGravity") "y"))]
    (session/put! :motion {:vx vx-new :vy vy-new})))

(if (aget js/window "DeviceMotionEvent")
  (aset js/window "ondevicemotion" motion))

(defn steer [steering-angle speed]
  (session/assoc-in! [:steering-angle] steering-angle)
  (session/assoc-in! [:steer-speed] speed))

(defn events->chan
  ([el event-type] (events->chan el event-type (chan)))
  ([el event-type c]
   (events/listen el event-type
                  (fn [e] (put! c e)))
   c))
(defn keyup-chan []
  (events->chan js/window goog.events.EventType.KEYUP
                (chan 1 (comp (map #(.-keyCode %))
                              (filter #{37 38 39 40})
                              (map {37 :steer-left-up
                                    38 :accelerate-up
                                    39 :steer-right-up
                                    40 :decelerate-up})))))

(defn keydown-chan []
  (events->chan js/window goog.events.EventType.KEYDOWN
                (chan 1 (comp (map #(.-keyCode %))
                              (filter #{37 38 39 40})
                              (map {37 :steer-left-down
                                    38 :accelerate-down
                                    39 :steer-right-down
                                    40 :decelerate-down})))))

(def motion-channel (async/merge [(keydown-chan) (keyup-chan) motion-chan] 1))



(go-loop []
         (let [event (<! motion-channel)]
           (println event)
           (case event
             :accelerate-down (session/assoc-in! [:motion :vy] 22)
             :accelerate-up (session/assoc-in! [:motion :vy] 0)
             :decelerate-down (session/assoc-in! [:motion :vy] -10)
             :decelerate-up (session/assoc-in! [:motion :vy] 0)
             :steer-left-down (steer MAX-STEER-ANGLE 1)
             :steer-left-up (steer 0.0 8)
             :steer-right-down (steer (* -1 MAX-STEER-ANGLE) 1)
             :steer-right-up (steer 0.0 8))
           (recur)))

(defn impulse [contact impulse]
  (-> contact
      (.GetFixtureA)
      (.GetBody)
      (.GetUserData)
      (println)))


(defn create-body [world options]
  (let [defaults {:density        1.0
                  :friction       0.0
                  :restitution    0.2
                  :linearDamping  0.2
                  :angularDamping 0.0
                  :gravityScale   1.0
                  :type           Box2D.Dynamics.b2Body.b2_dynamicBody}
        opts (merge defaults options)
        body-def (new Box2D.Dynamics.b2BodyDef)
        fix-def (new Box2D.Dynamics.b2FixtureDef)]


    (aset fix-def "density" (:density opts))
    (aset fix-def "friction" (:friction opts))
    (aset fix-def "restitution" (:restitution opts))
    (aset fix-def "gravityScale" (:gravityScale opts))
    (aset fix-def "shape" (:shape opts))
    (if (aget (aget fix-def "shape") "SetAsBox")
      (.SetAsBox (aget fix-def "shape") (:w opts) (:h opts)))
    (aset body-def "linearDamping" (:linearDamping opts))
    (aset body-def "angularDamping" (:angularDamping opts))
    (aset body-def "type" (:type opts))
    (aset body-def "userData" (:user-data opts))
    (.Set (aget body-def "position") (:x opts) (:y opts))
    (let [b (.CreateBody world body-def)]
      (.CreateFixture b fix-def)
      b)))

(defn revolute-joint [world car-body wheel]
  (let [joint-def (new Box2D.Dynamics.Joints.b2RevoluteJointDef)]
    (.Initialize joint-def car-body wheel (.GetWorldCenter wheel))
    (doto joint-def
      (aset "enableMotor" true)
      (aset "maxMotorTorque" 100000)
      (aset "enableLimit" true)
      (aset "lowerAngle" (* -1 MAX-STEER-ANGLE))
      (aset "upperAngle" MAX-STEER-ANGLE))

    (.CreateJoint world joint-def))
  )
(defn prismatic-joint [world body1 body2]
  (let [joint-def (new Box2D.Dynamics.Joints.b2PrismaticJointDef)]
    (.Initialize joint-def body1 body2 (.GetWorldCenter body2) (b2vec2 1 0))
    (doto joint-def
      (aset "enableLimit" true)
      (aset "upperTranslation" 0.0))
    (.CreateJoint world joint-def))
  )

(defn create-car [world]
  (let [car-pos (b2vec2 2 2)
        car-dim (b2vec2 0.2 0.35)
        wheel-dim (.Copy car-dim)
        x (.-x car-pos)
        y (.-y car-pos)
        car-dim-x (aget car-dim "x")
        car-dim-y (aget car-dim "y")
        car-body (create-body world {:x              x
                                     :y              y
                                     :w              car-dim-x
                                     :h              car-dim-y
                                     :shape          (new Box2D.Collision.Shapes.b2PolygonShape)
                                     :linearDamping  10.0
                                     :angularDamping 10.0
                                     :user-data      true})]
    (.Multiply wheel-dim 0.2)
    (let [wheel-dim-x (aget wheel-dim "x")
          wheel-dim-y (aget wheel-dim "y")
          wheel-pos-y-adjustment (* 1.5 wheel-dim-y)

          right-rear-wheel (create-body world
                                        {:x     (+ x car-dim-x)
                                         :y     (+ wheel-pos-y-adjustment (- y car-dim-y))
                                         :w     wheel-dim-x
                                         :h     wheel-dim-y
                                         :shape (new Box2D.Collision.Shapes.b2PolygonShape)})
          left-rear-wheel (create-body world
                                       {:x     (- x car-dim-x)
                                        :y     (+ wheel-pos-y-adjustment (- y car-dim-y))
                                        :w     wheel-dim-x
                                        :h     wheel-dim-y
                                        :shape (new Box2D.Collision.Shapes.b2PolygonShape)})
          left-wheel (create-body world
                                  {:x     (- x car-dim-x)
                                   :y     (- (+ y car-dim-y) wheel-pos-y-adjustment)
                                   :w     wheel-dim-x
                                   :h     wheel-dim-y
                                   :shape (new Box2D.Collision.Shapes.b2PolygonShape)})
          right-wheel (create-body world
                                   {:x     (+ x car-dim-x)
                                    :y     (- (+ y car-dim-y) wheel-pos-y-adjustment)
                                    :w     wheel-dim-x
                                    :h     wheel-dim-y
                                    :shape (new Box2D.Collision.Shapes.b2PolygonShape)})
          ]

      {:body                   car-body
       :left-wheel             left-wheel
       :right-wheel            right-wheel
       :left-rear-wheel        left-rear-wheel
       :right-rear-wheel       right-rear-wheel

       :left-wheel-joint       (revolute-joint world car-body left-wheel)
       :right-wheel-joint      (revolute-joint world car-body right-wheel)
       :left-rear-wheel-joint  (prismatic-joint world car-body left-rear-wheel)
       :right-rear-wheel-joint (prismatic-joint world car-body right-rear-wheel)
       :gear                   2})))


(defn create-world [context]
  (let [gravity (b2vec2 0 0)
        do-sleep true
        world (new Box2D.Dynamics.b2World gravity do-sleep)
        debug-draw (new Box2D.Dynamics.b2DebugDraw)]
    (doto debug-draw
      (.SetSprite context)
      (.SetDrawScale SCALE)
      (.SetFillAlpha 0.5)
      (.SetLineThickness 1.0)
      (.SetFlags (bit-or (aget Box2D.Dynamics.b2DebugDraw "e_shapeBit")
                         (aget Box2D.Dynamics.b2DebugDraw "e_jointBit")
                         ;(aget Box2D.Dynamics.b2DebugDraw "e_aabbBit")
                         ;(aget Box2D.Dynamics.b2DebugDraw "e_pairBit")
                         ;(aget Box2D.Dynamics.b2DebugDraw "e_centerOfMassBit")
                         )))
    (.SetDebugDraw world debug-draw)
    world))


(defn update-wheel [wheel gear]
  (let [direction (.Copy (-> wheel
                             (.GetTransform)
                             (aget "R")
                             (aget "col2")))]
    (.Multiply direction (* gear (/ (session/get-in [:motion :vy]) 10)))
    (.ApplyForce wheel direction (.GetPosition wheel))))

(defn update-wheel-joint [wheel-joint]
  (let [speed (* (- (session/get-in [:steering-angle]) (.GetJointAngle wheel-joint)) (session/get-in [:steer-speed]))]
    (println speed)
    (.SetMotorSpeed wheel-joint speed)))

(defn update-car [{:keys [left-wheel right-wheel left-wheel-joint right-wheel-joint gear]}]
    (update-wheel left-wheel gear)
    (update-wheel right-wheel gear)
    (update-wheel-joint left-wheel-joint)
    (update-wheel-joint right-wheel-joint))

(defn redraw-world [{:keys [context world canvas-height]}]
  (doto context
    (.save)
    (.translate 0 canvas-height)
    (.scale 1 -1))
  (.DrawDebugData world)
  (.restore context))

(defn update-world [game]
  (let [fps 60
        time-step (/ 1.0 fps)
        world (:world game)]
    (update-car (:car game))
    (.Step world time-step 8 3)
    (.ClearForces world)
    (redraw-world game)))



(defn create-borders [world]

  (let [screen-width (/ (:width canvas-attributes) SCALE)
        half-screen-width (/ (:width canvas-attributes) SCALE 2)
        screen-height (/ (:height canvas-attributes) SCALE)
        half-screen-height (/ (:height canvas-attributes) SCALE 2)
        static-body Box2D.Dynamics.b2Body.b2_staticBody]
    (create-body world {:x half-screen-width :y 0 :w half-screen-width :h 0.03 :type static-body :restitution 1 :user-data "n" :shape (new Box2D.Collision.Shapes.b2PolygonShape)})
    (create-body world {:x half-screen-width :y screen-height :w half-screen-width :h 0.03 :type static-body :restitution 1 :user-data "s" :shape (new Box2D.Collision.Shapes.b2PolygonShape)})

    (create-body world {:x 0 :y half-screen-height :w 0.03 :h half-screen-width :type static-body :restitution 1 :user-data "w" :shape (new Box2D.Collision.Shapes.b2PolygonShape)})
    (create-body world {:x screen-width :y half-screen-height :w 0.03 :h half-screen-width :type static-body :restitution 1 :user-data "e" :shape (new Box2D.Collision.Shapes.b2PolygonShape)})))

(defn collision [contact]
  (let [user-data-a (-> contact
                        (.GetFixtureA)
                        (.GetBody)
                        (.GetUserData))
        user-data-b (-> contact
                        (.GetFixtureB)
                        (.GetBody)
                        (.GetUserData))]

    (do
      (if (and (= user-data-a "puck") (= user-data-b "gw"))
        (session/update-in! [:scores :west] inc))
      (if (and (= user-data-b "gw") (= user-data-a "puck"))
        (comment (session/update-in! [:scores :west] inc)))
      (if (and (= user-data-a "puck") (= user-data-b "ge"))
        (session/update-in! [:scores :east] inc))
      (if (and (= user-data-b "ge") (= user-data-a "puck"))
        (session/update-in! [:scores :east] inc)))
    ))

(defn init [context]
  (let [world (create-world context)]
    (create-borders world)
    world))

(defn world-did-mount [canvas]
  (let [width (:width canvas-attributes)
        height (:height canvas-attributes)
        context (.getContext (reagent/dom-node canvas) "2d")
        world (init context)
        game {:context       context
              :world         world
              :car           (create-car world)
              :canvas-height height
              :canvas-width  width}
        goal-options {:type        Box2D.Dynamics.b2Body.b2_staticBody
                      :restitution 1.0}
        listener (new Box2D.Dynamics.b2ContactListener)]
    (doto listener
      (aset "BeginContact" collision)
      )
    ;;(aset "EndContact" collision)
    ;;(aset "PostSolve" impulse)
    ;;(aset "PreSolve" impulse)
    (.SetContactListener world listener)
    (create-body world (merge goal-options {:x 0 :y (/ (/ height 100) 2) :w 0.2 :h 1 :shape (new Box2D.Collision.Shapes.b2PolygonShape) :user-data "ge"}))
    (create-body world (merge goal-options {:x (/ width 100) :y (/ height 100 2) :w 0.2 :h 1 :shape (new Box2D.Collision.Shapes.b2PolygonShape) :user-data "gw"}))

    (let [free {:restitution    1.0
                :linearDamping  1.0
                :angularDamping 1.0
                :density        0.2
                :x              2.5
                :y              2.5
                :w              0.1
                :h              0.1
                :shape          (new Box2D.Collision.Shapes.b2PolygonShape)}

          circle {:x         2.5
                  :y         3
                  :user-data "puck2"
                  :shape     (new Box2D.Collision.Shapes.b2CircleShape 0.1)}]

      (create-body world (merge free {:user-data "puck"}))
      (create-body world (merge free circle)))
    (aset Box2D.Common.b2Settings "b2_velocityThreshold" 0.1)
    (reagent/next-tick
      (fn update []
        (update-world game)
        (reagent/next-tick update)))))











(defn racer []
  (reagent/create-class
    {:component-did-mount world-did-mount
     :display-name        "world"
     :reagent-render      (fn [] [:canvas canvas-attributes])}))

;[:div#info (str "x: " (session/get-in [:motion :vx]))]
;[:div#info (str "y: " (session/get-in [:motion :vy]))]

(defn racer-page []
  [:div
   [:div (str "speed " (/ (session/get-in [:motion :vy]) 10) ", scores: " (session/get-in [:scores :west]) "-" (session/get-in [:scores :east]))]
   [racer]])


(defn current-page []
  [:div [(session/get :current-page)]])

;; -------------------------
;; Routes
(secretary/set-config! :prefix "#")

(secretary/defroute "/" []
                    (session/put! :current-page #'racer-page))

;; -------------------------
;; History
;; must be called after routes have been defined
(defn hook-browser-navigation! []
  (doto (History.)
    (events/listen
      EventType/NAVIGATE
      (fn [event]
        (secretary/dispatch! (.-token event))))
    (.setEnabled true)))

;; -------------------------
;; Initialize app
(defn mount-root []
  (reagent/render [current-page] (.getElementById js/document "app")))

(defn init! []
  (hook-browser-navigation!)
  (mount-root))
