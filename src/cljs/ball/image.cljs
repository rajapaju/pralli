(ns ball.image)


(defn create-image [path]
  (let [car-img (new js/Image)]
    (set! (.-src car-img) path)
    car-img))